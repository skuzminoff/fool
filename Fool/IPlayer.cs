﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fool
{
	public interface IPlayer
	{
		string Name { get; }
		bool HasCards { get; }
		int CardsAmount { get; }

		List<Card> RememberedCards { get; set; }

		Hand Hand { get; set; }

		Card MakeTurn(double possibleCardCost, IList<Card> beaten, Card trump, int opponentCardsAmount);
		Card MakeTurnEndGame(IList<Card> beaten);
		bool BeatOrTake(Card card, double possibleCardCost, out Card cardToBeat);
		bool BeatOrTakeEndGame(Card card, out Card cardToBeat);
		Card ChooseCardToAddAfterOpponentTakesCard(IList<Card> cardsOnTheTable, double possibleCardCost);
		Card ChooseCardToAddAfterOpponentTakesCardEndGame(IList<Card> cardsOnTheTable);
		Card ChooseCardToAddAfterOpponentBeatsCard(IList<Card> cardsOnTheTable, double possibleCardCost);
		Card ChooseCardToAddAfterOpponentBeatsCardEndGame(IList<Card> cardsOnTheTable);
	}

}
