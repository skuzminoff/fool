﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fool
{
	public class Hand
	{
		internal IList<Card> Cards
		{
			get { return _cards; }
		}

		internal double Cost {
			get { return _cards.Sum(t => t.Cost)/Convert.ToDouble(_cards.Count); }
		}

		private readonly IList<Card> _cards;


		public Hand(IList<Card> cards)
		{
			_cards = cards;
		}

		public Hand(int[] handIndexes, List<Card> cards)
		{
			var hand = handIndexes.Select(index => cards[index-1]).ToList();
			_cards = hand;
		}

		public override string ToString()
		{
			string ret = "|" + string.Join("|", Cards);
			ret += "|" + " amount of cards:" + Cards.Count;
			return ret;
		}

		internal Hand CopyHand()
		{
			return new Hand(_cards.ToList());
		}


	}
}
