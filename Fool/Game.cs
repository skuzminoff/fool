﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;

namespace Fool
{
	public class Game 
	{

		private bool _player1Turn;

		public IList<Card> Deck;
		public IList<Card> Beaten;
		public IPlayer Player1;
		public IPlayer Player2;
		public Card Trump;
		private int _turnNumber;

		public static Logger Log = LogManager.GetCurrentClassLogger();

		public bool IsGameFinished
		{
			get { return !Player1.HasCards || !Player2.HasCards; }
		}

		public IPlayer Winner
		{
			get { return !Player1.HasCards ? Player1 : Player2; }
		}

		public Game(IPlayer player1, IPlayer player2)
		{
			Player1 = player1;
			Player2 = player2;
			Beaten = new List<Card>();
			_turnNumber = 0;
			Deck = Util.GenerateDeck();
			Util.ShuffleDeck(Deck);
			DistributeCards();
			_player1Turn = IsPlayer1First();
			PrintDeck();
		}

		public void PrintDeck()
		{
			NLog.Fluent.Log.Info("Trump: " + Trump);
			foreach (var c in Deck)
				Log.Info(c.ToString());
		}

		public void DistributeCards()
		{
			//define player.hand. bot.hand, trumpType

			IList<Card> p1Cards = new List<Card>();
			IList<Card> p2Cards = new List<Card>();

			int j = 0, k = 0;
			while (j + k < 12)
			{
				if ((j + k) % 2 == 0)
				{
					j++;
					var c = Util.GetRandomCard(Deck);
					p1Cards.Add(c);
					Deck.Remove(c);
				}
				else
				{
					k++;
					var c = Util.GetRandomCard(Deck);
					p2Cards.Add(c);
					Deck.Remove(c);
				}
			}

			Trump = Deck[Deck.Count - 1];
			foreach (Card t in Deck)
				t.IsTrump = t.Suite == Trump.Suite;

			Deck[Deck.Count - 1] = Deck[0];
			Deck[0] = Trump;

			Player1.Hand = new Hand(p1Cards);
			//Player1.RememberedCards.Add(Trump);
			Player2.Hand = new Hand(p2Cards);
			//Player2.RememberedCards.Add(Trump);
		}

		public bool IsPlayer1First()
		{
			var p1st = GetSmallestTrump(Player1.Hand, Trump.Suite);
			var p2st = GetSmallestTrump(Player2.Hand, Trump.Suite);
			// there is at least one trump at each player's hand
			if (p1st != null && p2st != null)
				return p1st.Rank < p2st.Rank;
			//only first player has trumps
			if (p1st != null)
				return true;
			//only second player has trump
			if (p2st != null)
				return false;
			//neither one has trumps - throw the dice =)
			int p1Luck = Util.RandomIndex(0, 100);
			int p2Luck = Util.RandomIndex(0, 100);
			return p1Luck > p2Luck;
		}

		public Card GetSmallestTrump(Hand hand, Suite cardSuite)
		{
			Card smallestTrump = null;
			foreach (Card t in hand.Cards)
			{
				if (t.Suite == cardSuite)
					if (smallestTrump == null)
						smallestTrump = t;
					else if (t.Rank < smallestTrump.Rank)
						smallestTrump = t;
			}
			return smallestTrump;
		}

		public void Turn()
		{
			bool isPlayer1First = _player1Turn;
			var cardsOnTheTable = new List<Card>();
			IPlayer ap;
			IPlayer dp;

			if (isPlayer1First)
			{
				ap = Player1;
				dp = Player2;
			}
			else
			{
				ap = Player2;
				dp = Player1;
			}

			string firstPlayer = isPlayer1First ? Player1.Name : Player2.Name;
			Log.Info("-----------------");
			Log.Info("Hands before Turn #" + _turnNumber + ":  ap:" + firstPlayer + " cards in deck: " + Deck.Count);
			Log.Info(Player1.Name + " " + Player1.Hand);
			Log.Info(Player2.Name + " " + Player2.Hand);

			if (Deck.Count == 0)
			{
				var attackCard = ap.MakeTurnEndGame(Beaten);

				Log.Info("Attack card : " + attackCard);

				if (PlayLocalEnd(ap, dp, cardsOnTheTable, attackCard))
				{
					//передаем ход противнику
					isPlayer1First = dp.Equals(Player1);
					//добавляем отбитые карты в отбой
					(Beaten as List<Card>).AddRange(cardsOnTheTable);

				}
				//если dp взял 
				else
				{
					//добавляем карты со стола на руку dp
					(dp.Hand.Cards as List<Card>).AddRange(cardsOnTheTable);
					//оставляем ход у того же игрока
					isPlayer1First = ap.Equals(Player1);
				}
			}
			else
			{
				var attackCard = ap.MakeTurn(Util.GetRandomCardMO(ap.Hand.Cards, ap.RememberedCards, Trump ), Beaten, Trump, dp.CardsAmount);
				Log.Info("Attack card : " + attackCard);
				//если dp отзащищался успешно
				if (PlayLocal(ap, dp, cardsOnTheTable, attackCard))
				{
					//передаем ход противнику
					isPlayer1First = dp.Equals(Player1);
					// запоминаем вышедшие карты для ap
					ap.RememberedCards .AddRange(cardsOnTheTable);
					
					// запоминаем вышедшие карты для	dp
					dp.RememberedCards.AddRange(cardsOnTheTable);

					//добавляем отбитые карты в отбой
					(Beaten as List<Card>).AddRange(cardsOnTheTable);
				}
				//если dp взял 
				else
				{

					// запоминаем вышедшие карты для ap
					ap.RememberedCards.AddRange(cardsOnTheTable);
					// запоминаем вышедшие карты для	dp
					dp.RememberedCards.AddRange(cardsOnTheTable);

					//добавляем карты со стола на руку dp
					(dp.Hand.Cards as List<Card>).AddRange(cardsOnTheTable);
					//оставляем ход у того же игрока
					isPlayer1First = ap.Equals(Player1);
				}
			}
			//убираем сыгранные карты из колоды
			Log.Info("Cards on the table : " + String.Join("|", cardsOnTheTable));
			Deck = Deck.Except(cardsOnTheTable).ToList();
			cardsOnTheTable.Clear();

			Log.Info("Hands after Turn #" + _turnNumber + ": ");
			Log.Info(Player1.Name + " " + Player1.Hand);
			Log.Info(Player2.Name + " " + Player2.Hand);

			AddCardsToHandAfterTurn(ap);
			AddCardsToHandAfterTurn(dp);

			_turnNumber++;

			_player1Turn = isPlayer1First;
		}

		private void AddCardsToHandAfterTurn(IPlayer ap)
		{
			int apCardsAmount = ap.CardsAmount;
			if (apCardsAmount < 6)
			{
				for (int i = 0; i + apCardsAmount < 6 && Deck.Count > 0; i++)
				{
					var c = Deck.Last();
					ap.Hand.Cards.Add(c);
					Deck.Remove(c);
				}
			}
		}

		//true - dp has successfully defended, false - unsuccessfully
		internal bool PlayLocal(IPlayer ap, IPlayer dp, IList<Card> cardsOnTheTable, Card attackCard)
		{
			Card defenseCard;
			cardsOnTheTable.Add(attackCard);
			while (dp.BeatOrTake(attackCard, Util.GetRandomCardMO(dp.Hand.Cards, dp.RememberedCards, Trump), out defenseCard))
			{
				Log.Info("Defense card : " + defenseCard);
				cardsOnTheTable.Add(defenseCard);
				attackCard = ap.ChooseCardToAddAfterOpponentBeatsCard(cardsOnTheTable, Util.GetRandomCardMO(ap.Hand.Cards, ap.RememberedCards, Trump));
				Log.Info("Attack card : " + attackCard);
				if (attackCard == null) return true;
				cardsOnTheTable.Add(attackCard);
			}

			while ((attackCard = ap.ChooseCardToAddAfterOpponentTakesCard(cardsOnTheTable, Util.GetRandomCardMO(ap.Hand.Cards, ap.RememberedCards, Trump))) != null)
			{
				Log.Info("Attack card : " + attackCard);
				cardsOnTheTable.Add(attackCard);
			}

			return false;
		}

		internal bool PlayLocalEnd(IPlayer ap, IPlayer dp, IList<Card> cardsOnTheTable, Card attackCard)
		{
			Card defenseCard;
			cardsOnTheTable.Add(attackCard);
			while (dp.BeatOrTakeEndGame(attackCard, out defenseCard))
			{
				Log.Info("Defense card : " + defenseCard);
				cardsOnTheTable.Add(defenseCard);
				attackCard = ap.ChooseCardToAddAfterOpponentBeatsCardEndGame(cardsOnTheTable);
				Log.Info("Attack card : " + attackCard);
				if (attackCard == null) return true;
				cardsOnTheTable.Add(attackCard);
			}

			while ((attackCard = ap.ChooseCardToAddAfterOpponentTakesCardEndGame(cardsOnTheTable)) != null)
			{
				Log.Info("Attack card : " + attackCard);
				cardsOnTheTable.Add(attackCard);
			}

			return false;
		}

	}

}


