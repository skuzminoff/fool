﻿using System;
using Fool.Players;

namespace Fool
{
	class Program
	{
		static void Main(string[] args)
		{
			var player1 = new Player(null, "Advanced bot");
			var player2 = new SimpleBot(null, "Simple bot 2"); 
			Game game;
			int player1Wins = 0;
			int player2Wins = 0;
			int numberofGames = 100;
			for (int i = 0; i < numberofGames; i++)
			{
				game = new Game(player1, player2);
				while (!game.IsGameFinished)
					game.Turn();
				if (game.Winner == player1)
					player1Wins++;
				else
					player2Wins++;


				game = null;
				player1.Hand = null;
				player2.Hand = null;
				player1.RememberedCards.Clear();
				player2.RememberedCards.Clear();
			}
			Console.WriteLine(player1.Name + " wins " + player1Wins + " times. Winrate(%) :" + player1Wins*100m/numberofGames);
			Console.WriteLine(player2.Name + " wins " + player2Wins + " times. Winrate(%) :" + player2Wins*100m/numberofGames);


			Console.ReadLine();
		}
	}
}
