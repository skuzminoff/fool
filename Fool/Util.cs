﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Fool
{
	public static class Util
	{
		private static readonly Random _random;

		static Util()
		{
			_random = new Random();
		}

		public static int RandomIndex(int leftBound, int rightBound)
		{
			return _random.Next(leftBound, rightBound);
		}

		public static double GenerateRandomHand(IList<Card> beaten, IList<Card> ownHand, Card trump, IList<Card> rememberedCards, int amountOfCards)
		{
			var fullDeck = GenerateDeck();

			//пробуем вычислить карты на руки у соперника. в списке rememberedCards хранятся все карты, бывшие на столе во время игры
			//находим разницу этого списка с отбоем - это даст нам часть карт на руке оппонента
			List<Card> opponentCards = rememberedCards.Distinct().Except(beaten.Union(ownHand)).ToList();

			//если карт в колоде нет и открытый козырь не у нас на руках => он в руке соперника
			if (beaten.Count + ownHand.Count + amountOfCards == 36 && !ownHand.Contains(trump))
				opponentCards.Add(trump);

			//убираем из множества для формирования сочетаний отбой и собственную руку
			var remainingCards = fullDeck.Except(beaten.Union(ownHand)).ToList();
			//убираем из множества для формирования сочетаний карты, которые точно есть в руке соперника
			remainingCards = remainingCards.Except(opponentCards).ToList();

		  int remainingCardsAmount = remainingCards.Count;
			//для выборки используем массив, размерность которого равна неизвестным картам на руке соперника
			int oppositeHandLenght = amountOfCards - opponentCards.Count;
			var handIndexes = new int[oppositeHandLenght];

			for (int i = 1; i <= oppositeHandLenght; i++)
				handIndexes[i - 1] = i;

			double mo = 0;
			double amount = Math.Round(NumberOfCombination(remainingCardsAmount, oppositeHandLenght));
			int p = oppositeHandLenght;
			while (p >= 1)
			{
				double opponentCardsCost = 0;

				double handCost = handIndexes.Sum(index => remainingCards[index - 1].Cost) + opponentCards.Sum(t => t.Cost);
				mo += handCost/(amountOfCards * amount);
				if (handIndexes[oppositeHandLenght - 1] == remainingCardsAmount)
					p = p - 1;
				else
					p = oppositeHandLenght;
				if (p < 1) continue;
				for (int i = oppositeHandLenght; i >= p; i--)
					handIndexes[i - 1] = handIndexes[p - 1] + i - p + 1;
			}

			return mo;
		}

		public static Hand GetOppositeHand(IList<Card> beaten, IList<Card> ownHand)
		{
			var fullDeck = GenerateDeck();
			var remainingCards = fullDeck.Except(beaten.Union(ownHand)).ToList();
			return new Hand(remainingCards);
		}

		public static double GetRandomCardMO(IList<Card> ownHand, IList<Card> rememberedCards, Card trump)
		{
			var fullDeck = GenerateDeck();
			var remainingCards = fullDeck.Except(rememberedCards.Distinct().Union(ownHand)).ToList();
			remainingCards.Remove(trump);

			double cardProbability = 1.0/remainingCards.Count;
			double mo = remainingCards.Sum(t => t.Cost*cardProbability);
			return mo;
		}

		public static Card GetRandomCard(IList<Card> deck, IList<Card> additional = null)
		{
			var set = new List<Card>();
			set.AddRange(deck);
			if (additional != null)
				set.AddRange(additional);
			return set[RandomIndex(0, set.Count - 1)];
		}

		public static IList<Card> GenerateDeck()
		{
			var list = new List<Card>();
			foreach (Suite s in Enum.GetValues(typeof (Suite)))
			{
				foreach (Rank r in Enum.GetValues(typeof (Rank)))
				{
					var card = new Card(r, s);
					list.Add(card);
				}
			}
			ShuffleDeck(list);
			return list;
		}

		public static void ShuffleDeck(IList<Card> deck)
		{
			for (int i = 0; i < deck.Count(); i++)
			{
				var j = RandomIndex(0, deck.Count());
				var element = deck[i];
				deck[i] = deck[j];
				deck[j] = element;
			}
		}

		private static double NumberOfCombination(int n, int k)
		{
			return Factorial(n) / (Factorial(k) * Factorial(n - k));
		}

		private static double Factorial(int n)
		{
			if (n == 0)
				return 1;
			return n * Factorial(n - 1);
		}

	}
}
