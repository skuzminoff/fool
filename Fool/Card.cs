﻿using System;

namespace Fool
{
	public enum Suite
	{
		Spades,
		Clubs,
		Diamonds,
		Hearts
	}

	public enum Rank
	{
		Six,
		Seven,
		Eight,
		Nine,
		Ten,
		Jack,
		Queen,
		King,
		Ace
	}

	public class Card : IEquatable<Card>
	{
		public bool IsTrump;
		public readonly Rank Rank;
		public readonly Suite Suite;

        // Вычислять один раз, все проверки бэд, миддл и др могут обойтись без поля IsTrump.
		public int Cost
		{
			get
			{
				switch (Rank)
				{
					case Rank.Ace:
						return (IsTrump) ? 18 : 9;
					case Rank.King:
						return (IsTrump) ? 17 : 8;
					case Rank.Queen:
						return (IsTrump) ? 16 : 7;
					case Rank.Jack:
						return (IsTrump) ? 15 : 6;
					case Rank.Ten:
						return (IsTrump) ? 14 : 5;
					case Rank.Nine:
						return (IsTrump) ? 13 : 4;
					case Rank.Eight:
						return (IsTrump) ? 12 : 3;
					case Rank.Seven:
						return (IsTrump) ? 11 : 2;
					case Rank.Six:
						return (IsTrump) ? 10 : 1;
				}
				return 0;
			}
		}


		public Card(Rank rank, Suite suite, bool isTrump = false)
		{
			Rank = rank;
			Suite = suite;
		}

		public override string ToString()
		{
			return Rank.ToString() + Suite.ToString();
		}

		public bool Equals(Card otherCard)
		{
			//Check whether the compared object is null.
			if (Object.ReferenceEquals(otherCard, null)) return false;

			//Check whether the compared object references the same data.
			if (Object.ReferenceEquals(this, otherCard)) return true;

			//Check whether the products' properties are equal.
			return Rank.Equals(otherCard.Rank) && Suite.Equals(otherCard.Suite);
		}

		public override int GetHashCode()
		{

			//Get hash code for the Name field if it is not null.
			int hashRank =  Rank.GetHashCode();

			//Get hash code for the Code field.
			int hashSuite = Suite.GetHashCode();

			//Calculate the hash code for the product.
			return hashRank ^ hashSuite;
		}

	}
}
